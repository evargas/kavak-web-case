jQuery(function(){
    setTimeout(WOW().init(), 1000);
    // animacion con delay
    jQuery(".wow-delay").children().each(function(index, el) {
        var tiempo;
        tiempo = (index*0.15);
        jQuery(this).attr("data-wow-delay", tiempo+'s');
    });

})
