<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo strtoupper($_SESSION['login']['lms_user']); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php 
        echo form_open('/'.$module.'/buscar/', array('class'=>'sidebar-form'));
        $buscar = array(
         'name'=>'buscar', 
         'placeholder'=>'Buscador', 
         'class'=>'form-control', 
         'type'=>'text', 
         'id'=>'buscar'
        );
      ?>
        <div class="input-group">
          <?php echo form_input($buscar); ?>
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      <?php echo form_close(); ?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Personas <small class="label bg-aqua"><?php echo $this->db->count_all('persona'); ?></small></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php base_url('persona') ?>"><i class="fa fa-circle-o"></i> Ver Personas</a></li>
            <li><a href="<?php base_url('persona/nuevo') ?>"><i class="fa fa-circle-o"></i> Registrar Persona</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Usuarios <small class="label bg-yellow"><?php echo $this->db->count_all('usuario'); ?></small></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php base_url('usuario') ?>"><i class="fa fa-circle-o"></i> Ver Usuarios</a></li>
            <li><a href="<?php base_url('usuario/nuevo') ?>"><i class="fa fa-circle-o"></i> Registrar Usuario</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-star"></i> <span>Productos <small class="label bg-green"><?php echo $this->db->count_all('producto'); ?></small></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php base_url('producto') ?>"><i class="fa fa-circle-o"></i> Ver Productos</a></li>
            <li><a href="<?php base_url('producto/nuevo') ?>"><i class="fa fa-circle-o"></i> Registrar Producto</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>Ventas <small class="label bg-red">proximamente</small></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php base_url('persona') ?>"><i class="fa fa-circle-o"></i> Ver Ventas</a></li>
            <li><a href="<?php base_url('persona/nuevo') ?>"><i class="fa fa-circle-o"></i> Registrar Ventas</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>