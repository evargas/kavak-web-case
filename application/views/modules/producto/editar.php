<div class="content-wrapper well">
   <h1>Actualización de Producto</h1>
   <div class="row">
      <div class="col-sm-6">
         <table class="table table-hover table-striped table-bordered">
            <tbody>
               <?php 
               $form = array(
                   'enctype'=>'multipart/form-data',
                  );
                  echo form_open('/producto/update_producto/'.$producto[0]->id_producto, $form);
                  
                  $nick = array(
                     'name'=>'nombre', 
                     'placeholder'=>'Nombre', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'nombre', 
                     'value'=>$producto[0]->nombre_producto
                  );
                  $descripcion = array(
                     'name'=>'descripcion', 
                     'placeholder'=>'Descripción', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'descripcion', 
                     'value'=>$producto[0]->descripcion_producto
                  );
                  $precio = array(
                     'name'=>'precio', 
                     'placeholder'=>'Precio', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'precio', 
                     'value'=>$producto[0]->precio
                  );
                  $stock = array(
                     'name'=>'stock', 
                     'placeholder'=>'stock', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'stock', 
                     'value'=>$producto[0]->stock
                  );
                  $creado['0'] = 'Seleccione';
                  foreach ($usuarios as $user) {
                     $creado[$user->id_usuario] = $user->nick; 
                  }

                   $fecha_creacion = array(
                     'name'=>'fecha_creacion', 
                     'placeholder'=>'Fecha de Creación', 
                     'class'=>'form-control', 
                     'type'=>'date', 
                     'id'=>'fecha_creacion', 
                     'value'=>$producto[0]->fecha_creacion
                  );

                  $modificado['0'] = 'Seleccione';
                  foreach ($usuarios as $user) {
                     $modificado[$user->id_usuario] = $user->nick; 
                  }

                  $fecha_modificacion = array(
                     'name'=>'fecha_modificacion', 
                     'placeholder'=>'Fecha de Modificación', 
                     'class'=>'form-control', 
                     'type'=>'date', 
                     'id'=>'fecha_modificacion', 
                     'value'=>$producto[0]->fecha_modificacion
                  );

                  $submit = array(
                     'name'=>'submit', 
                     'class'=>'btn btn-info', 
                     'value'=>'Actualizar producto'
                  );
               ?>
               <tr>
                  <td><?php echo form_label('Nombre de producto: ', 'nick'); ?></td>
                  <td><?php echo form_input($nick); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Descripcion: ', 'descripcion'); ?></td>
                  <td><?php echo form_input($descripcion); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Imagen: ', 'foto'); ?></td>
                  <td>
                     <img src="<?php echo $producto[0]->image ?>" class="img-responsive img-thumbnail" width="90px">
                     <input type="file" name="foto" id="foto">
                  </td>
               </tr>
               <tr>
                  <td><?php echo form_label('Precio: ', 'precio'); ?></td>
                  <td><?php echo form_input($precio); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Stock: ', 'stock'); ?></td>
                  <td><?php echo form_input($stock); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Creado por: ', 'creado_por'); ?></td>
                  <td><?php echo form_dropdown('creado_por', $creado, $producto[0]->creado_por, array('class'=>'form-control')); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Fecha de Creación: ', 'fecha_creacion'); ?></td>
                  <td><?php echo form_input($fecha_creacion); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Modificado por: ', 'modificado_por'); ?></td>
                  <td><?php echo form_dropdown('modificado_por', $modificado, $producto[0]->modificado_por, array('class'=>'form-control')); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Fecha de Modificación: ', 'fecha_modificacion'); ?></td>
                  <td><?php echo form_input($fecha_modificacion); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_submit($submit); ?></td>
                  <td></td>
               </tr>
               <?php 
                  echo form_close();
               ?>
            </tbody>
         </table>
      </div>
   </div>
</div>