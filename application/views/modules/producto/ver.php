<div  class="content-wrapper well">
	<h1>Productos</h1>
	<h2>Total <?php echo $this->db->affected_rows(); ?> 
		<a class="btn btn-success" href="<?php echo base_url('producto/nuevo'); ?>">Nuevo</a>
	</h2>
	<?php 
		if ($productos!=false) {
			?>
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Precio</th>
						<th>Stock</th>
						<th>Creado Por</th>
						<th>Fecha de Creación</th>
						<th>Modificado por</th>
						<th>Fecha de Modificación</th>
						<th>Editar</th>
						<th>Borrar</th>
					</tr>
				</thead>
				<tbody>
	   			<?php
		   			if (count($productos)>1) {
						foreach ($productos as $producto) {
							 printf('<tr>
							 			<td>
								 			<a class="badge badge-info" href="'.base_url('producto/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a class="label label-primary" href="'.base_url('producto/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
				   						<td>
				   							<a class="label label-warning" href="'.base_url('producto/editar/%s').'">Editar</a>
				   						</td>
				   						<td>
				   							<a class="label label-danger" href="'.base_url('producto/borrar/%s').'">Borrar</a>
				   						</td>
							 		</tr>',
					        		$producto->id_producto,
					        		$producto->id_producto,
					        		$producto->id_producto,
						        	$producto->nombre_producto,
						        	$producto->descripcion_producto,  
						        	$producto->precio,
						        	$producto->stock,
						        	$producto->creado_por, 
						        	$producto->fecha_creacion,
						        	$producto->modificado_por, 
						        	$producto->fecha_modificacion, 
						        	$producto->id_producto,
					        		$producto->id_producto 
					        	);
						}
		   			}else{
		   				printf('<tr>
							 			<td>
								 			<a class="badge badge-info" href="'.base_url('producto/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a class="label label-primary" href="'.base_url('producto/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
				   						<td>
				   							<a class="label label-warning" href="'.base_url('producto/editar/%s').'">Editar</a>
				   						</td>
				   						<td>
				   							<a class="label label-danger" href="'.base_url('producto/borrar/%s').'">Borrar</a>
				   						</td>
							 		</tr>',
					        		$productos[0]->id_producto,
					        		$productos[0]->id_producto,
					        		$productos[0]->id_producto,
						        	$productos[0]->nombre_producto,
						        	$productos[0]->descripcion_producto,  
						        	$productos[0]->precio,
						        	$productos[0]->stock,
						        	$productos[0]->creado_por, 
						        	$productos[0]->fecha_creacion,
						        	$productos[0]->modificado_por, 
						        	$productos[0]->fecha_modificacion, 
						        	$productos[0]->id_producto,
					        		$productos[0]->id_producto 
					        	);
		   				echo '<tr>
		   						<td>
									<a  class="btn btn-info" href="'.base_url('producto').'">Ver Todos
									</a>
		   						</td>
		   					</tr>';
		   			}
	   			?>
			</tbody>
		</table>
			<?php
		}else{
			echo '<h2>No se encontraron productos</h2>';
		}
	?>
</div>