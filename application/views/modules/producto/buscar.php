<fieldset>
	<legend>Buscador</legend>

	<?php 
		echo form_open('/producto/buscar/');

		$buscar = array(
		 'name'=>'buscar', 
		 'placeholder'=>'Buscador', 
		 'class'=>'form-control', 
		 'type'=>'text', 
		 'id'=>'buscar'
		);
		$submit = array(
		 'name'=>'submit', 
		 'class'=>'btn btn-default', 
		 'value'=>'Buscar Producto'
		);
		echo form_label('¿Qué producto estas buscando?: ', 'buscar');
		echo form_input($buscar);
		echo '<br>';
		echo form_submit($submit); 

		echo form_close();
	?>
</fieldset>