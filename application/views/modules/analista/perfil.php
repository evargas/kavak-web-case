<div  class="content-wrapper well">
	<h1>Listado de Usuarios</h1>
	<h2>Total <?php echo $this->db->affected_rows(); ?> 
		<a class="btn btn-success" href="<?php echo base_url('usuario/nuevo'); ?>">Nuevo</a>
	</h2> 
	<?php 
		if ($usuarios!=false) {
			?>
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>ID Persona</th>
						<th>Nick</th>
						<th>Pass</th>
						<th>Rol</th>
						<th>Activo</th>
						<th>Creado</th>
						<th>Fecha de Creación</th>
						<th>Editado</th>
						<th>Fecha de Modificación</th>
						<th>Editar</th>
						<th>Borrar</th>
					</tr>
				</thead>
				<tbody>
	   			<?php
		   			if (count($usuarios)>1) {
						foreach ($usuarios as $usuario) {
							switch ($usuario->rol) {
								case '2':
									$rol = 'Adinistrador'; 
									break;
								case '1':
									$rol = 'Analista'; 
									break;
								default:
									$rol = 'Sin Rol Asignado';
									break;
							}
							switch ($usuario->activo) {
								case '2':
									$activo = 'Por Aprobación'; 
									break;
								case '1':
									$activo = 'Activo'; 
									break;
								default:
									$activo = 'Inactivo';
									break;
							}
							 printf('<tr>
							 			<td>
								 			<a class="badge badge-info" href="'.base_url('usuario/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a class="label label-primary" href="'.base_url('usuario/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a href="'.base_url('usuario/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
				   						<td>
				   							<a class="label label-warning" href="'.base_url('usuario/editar/%s').'">Editar</a>
				   						</td>
				   						<td>
				   							<a class="label label-danger" href="'.base_url('usuario/borrar/%s').'">Borrar</a>
				   						</td>
							 		</tr>',
					        		$usuario->id_usuario,
					        		$usuario->id_usuario,
					        		$usuario->id_persona,
					        		$usuario->id_persona,
					        		$usuario->id_usuario,
						        	$usuario->nick,
						        	$usuario->pass,
						        	$rol,  
						        	$activo,
						        	$usuario->creado_por, 
						        	$usuario->fecha_creacion,
						        	$usuario->modificado_por, 
						        	$usuario->fecha_modificacion, 
						        	$usuario->id_usuario,
					        		$usuario->id_usuario 
					        	);
						}
		   			}else{
		   				switch ($usuarios[0]->rol) {
								case '2':
									$rol = 'Adinistrador'; 
									break;
								case '1':
									$rol = 'Analista'; 
									break;
								default:
									$rol = 'Falta Rol';
									break;
							}
							switch ($usuarios[0]->activo) {
								case '2':
									$activo = 'Stand By'; 
									break;
								case '1':
									$activo = 'Activo'; 
									break;
								default:
									$activo = 'Inactivo';
									break;
							}
		   				printf('<tr>
							 			<td>
								 			<a class="badge badge-info" href="'.base_url('usuario/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a class="label label-primary" href="'.base_url('usuario/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a href="'.base_url('usuario/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
				   						<td>
				   							<a class="label label-warning" href="'.base_url('usuario/editar/%s').'">Editar</a>
				   						</td>
				   						<td>
				   							<a class="label label-danger" href="'.base_url('usuario/borrar/%s').'">Borrar</a>
				   						</td>
							 		</tr>',
					        		$usuarios[0]->id_usuario,
					        		$usuarios[0]->id_usuario,
					        		$usuarios[0]->id_persona,
					        		$usuarios[0]->id_persona,
					        		$usuarios[0]->id_usuario,
						        	$usuarios[0]->nick,
						        	$usuarios[0]->pass,
						        	$usuarios[0]->rol,  
						        	$usuarios[0]->activo,
						        	$usuarios[0]->creado_por, 
						        	$usuarios[0]->fecha_creacion,
						        	$usuarios[0]->modificado_por, 
						        	$usuarios[0]->fecha_modificacion, 
						        	$usuarios[0]->id_usuario,
					        		$usuarios[0]->id_usuario 
					        	);
		   				echo '<tr>
		   						<td>
									<a  class="btn btn-info" href="'.base_url('usuario').'">Ver Todos
									</a>
		   						</td>
		   					</tr>';
		   			}
	   			?>
			</tbody>
		</table>
			<?php
		}else{
			echo '<h2>No se encontraron usuarios</h2>';
		}
	?>
</div>