<div  class="content-wrapper well">
	<h1>Bienvenido</h1>
	<div class="row wow-delay">
		<div class="col-md-6 animated zoomInUp">
		<a href="<?php echo base_url('analista/perfil'); ?>">
			<div class="info-box bg-yellow">
			  <span class="info-box-icon"><i class="fa fa-star"></i></span>
			  <div class="info-box-content">
			    <span class="info-box-text">Perfil</span>
			    <span class="info-box-number"><?php echo $_SESSION['login']['lms_nombre'] ?></span>
			    <!-- The progress section is optional -->
			    <div class="progress">
			      <div class="progress-bar" style="width: 70%"></div>
			    </div>
			    <span class="progress-description">
			      70% Increase in 30 Days
			    </span>
			  </div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
			</a>
		</div>
		<div class="col-md-6 animated zoomInUp">
		<a href="<?php echo base_url('analista/productos'); ?>">
			<div class="info-box bg-green">
			  <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>
			  <div class="info-box-content">
			    <span class="info-box-text">Productos</span>
			    <span class="info-box-number"><?php echo $this->db->affected_rows(); ?></span>
			    <!-- The progress section is optional -->
			    <div class="progress">
			      <div class="progress-bar" style="width: 90%"></div>
			    </div>
			    <span class="progress-description">
			      90% Increase in 30 Days
			    </span>
			  </div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
			</a>
		</div>
	</div>
</div>