<div  class="content-wrapper well">
   <h1>Editar Persona</h1>
   <div class="row">
      <div class="col-sm-6">
         <table class="table table-hover table-striped table-bordered">
               <tbody>
                  <?php 
                     echo form_open('/persona/update_persona/'.$personas[0]->id_persona);
                     
                     $p_nombre = array(
                        'name'=>'p_nombre', 
                        'placeholder'=>'Primer Nombre', 
                        'class'=>'form-control', 
                        'type'=>'text', 
                        'id'=>'p_nombre', 
                        'value'=>$personas[0]->p_nombre
                     );
                     $s_nombre = array(
                        'name'=>'s_nombre', 
                        'placeholder'=>'Segundo Nombre', 
                        'class'=>'form-control', 
                        'type'=>'text', 
                        'id'=>'s_nombre', 
                        'value'=>$personas[0]->s_nombre
                     );
                     $a_paterno = array(
                        'name'=>'a_paterno', 
                        'placeholder'=>'Primer Apellido', 
                        'class'=>'form-control', 
                        'type'=>'text', 
                        'id'=>'a_paterno', 
                        'value'=>$personas[0]->a_paterno
                     );
                     $a_materno = array(
                        'name'=>'a_materno', 
                        'placeholder'=>'Segundo Apellido', 
                        'class'=>'form-control', 
                        'type'=>'text', 
                        'id'=>'a_materno', 
                        'value'=>$personas[0]->a_materno
                     );
                     $ci = array(
                        'name'=>'ci', 
                        'placeholder'=>'CI', 
                        'class'=>'form-control', 
                        'type'=>'number', 
                        'id'=>'ci', 
                        'value'=>$personas[0]->ci
                     );
                     $email = array(
                        'name'=>'email', 
                        'placeholder'=>'Email', 
                        'class'=>'form-control', 
                        'type'=>'email', 
                        'id'=>'email', 
                        'value'=>$personas[0]->email
                     );
                     $telefono = array(
                        'name'=>'telefono', 
                        'placeholder'=>'Teléfono', 
                        'class'=>'form-control', 
                        'type'=>'text', 
                        'id'=>'telefono', 
                        'value'=>$personas[0]->telefono
                     );
                     $fecha_nacimiento = array(
                        'name'=>'fecha_nacimiento', 
                        'placeholder'=>'Fecha de Nacimiento', 
                        'class'=>'form-control', 
                        'type'=>'date', 
                        'id'=>'fecha_nacimiento', 
                        'value'=>$personas[0]->fecha_nacimiento
                     );
                     $submit = array(
                        'name'=>'submit', 
                        'class'=>'btn btn-warning', 
                        'value'=>'Editar Persona'
                     );
                  ?>
                  <tr>
                     <td><?php echo form_label('Primer Nombre: ', 'p_nombre'); ?></td>
                     <td><?php echo form_input($p_nombre); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_label('Segundo Nombre: ', 's_nombre'); ?></td>
                     <td><?php echo form_input($s_nombre); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_label('Primer Apellido: ', 'a_paterno'); ?></td>
                     <td><?php echo form_input($a_paterno); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_label('Segundo Apellido: ', 'a_materno'); ?></td>
                     <td><?php echo form_input($a_materno); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_label('CI: ', 'ci'); ?></td>
                     <td><?php echo form_input($ci); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_label('Email: ', 'email'); ?></td>
                     <td><?php echo form_input($email); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_label('Teléfono: ', 'telefono'); ?></td>
                     <td><?php echo form_input($telefono); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_label('Fecha de Nacimiento: ', 'fecha_nacimiento'); ?></td>
                     <td><?php echo form_input($fecha_nacimiento); ?></td>
                  </tr>
                  <tr>
                     <td><?php echo form_submit($submit); ?></td>
                     <td></td>
                  </tr>
                  <?php 
                     echo form_close();
                  ?>
               </tbody>

         </table>
      </div>
   </div>
</div>