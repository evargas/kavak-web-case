<div  class="content-wrapper well">
	<h1>Personas Registradas</h1>
	<h2>Total <?php echo $this->db->affected_rows(); ?> 
		<a class="btn btn-success" href="<?php echo base_url('persona/nuevo'); ?>">Nuevo</a>
	</h2> 
	<?php 
		if ($personas!=false) {
			?>
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Primer Nombre</th>
						<th>Segundo Nombre</th>
						<th>Primer Apellido</th>
						<th>Segundo Apellido</th>
						<th>CI</th>
						<th>Email</th>
						<th>Teléfono</th>
						<th>Fecha de Nacimiento</th>
						<th>Fecha de Creación</th>
						<th>Editar</th>
						<th>Borrar</th>
					</tr>
				</thead>
				<tbody>
	   			<?php
		   			if (count($personas)>1) {
						foreach ($personas as $persona) {
							 printf('<tr>
							 			<td>
								 			<a class="badge badge-info" href="'.base_url('persona/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a class="label label-primary" href="'.base_url('persona/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
				   						<td>
				   							<a class="label label-warning" href="'.base_url('persona/editar/%s').'">Editar</a>
				   						</td>
				   						<td>
				   							<a class="label label-danger" href="'.base_url('persona/borrar/%s').'">Borrar</a>
				   						</td>
							 		</tr>',
					        		$persona->id_persona,
					        		$persona->id_persona,
					        		$persona->id_persona,
						        	$persona->p_nombre,
						        	$persona->s_nombre,  
						        	$persona->a_paterno,
						        	$persona->a_materno, 
						        	$persona->ci,
						        	$persona->email, 
						        	$persona->telefono, 
						        	$persona->fecha_nacimiento,
						        	$persona->fecha_creacion,
					        		$persona->id_persona,
					        		$persona->id_persona
					        	);
						}
		   			}else{
		   				printf('<tr>
							 			<td>
								 			<a class="badge badge-info" href="'.base_url('persona/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<a class="label label-primary" href="'.base_url('persona/index/%s').'">
								 				<b>%s</b>
								 			</a> 
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<b>%s</b>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
							 			<td>
								 			<span>%s</span>
							 			</td>
				   						<td>
				   							<a class="label label-warning" href="'.base_url('persona/editar/%s').'">Editar</a>
				   						</td>
				   						<td>
				   							<a class="label label-danger" href="'.base_url('persona/borrar/%s').'">Borrar</a>
				   						</td>
							 		</tr>',
					        		$personas[0]->id_persona,
					        		$personas[0]->id_persona,
					        		$personas[0]->id_persona,
						        	$personas[0]->p_nombre,
						        	$personas[0]->s_nombre,  
						        	$personas[0]->a_paterno,
						        	$personas[0]->a_materno, 
						        	$personas[0]->ci,
						        	$personas[0]->email, 
						        	$personas[0]->telefono, 
						        	$personas[0]->fecha_nacimiento,
						        	$personas[0]->fecha_creacion,
					        		$personas[0]->id_persona,
					        		$personas[0]->id_persona
					        	);
		   			  echo '<tr>
		   						<td>
									<a  class="btn btn-info" href="'.base_url('persona').'">Ver Todos
									</a>
		   						</td>
		   					</tr>';
		   			}
	   			?>
				</tbody>
			</table>
			<?php
		}else{
			echo '<h2>No se encontraron personas</h2>';
		}
	?>
</div>