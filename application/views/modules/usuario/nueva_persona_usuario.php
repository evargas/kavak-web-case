<div  class="content-wrapper well">
   <h1>Nuevo Persona / Usuario</h1>
   <div class="row">
      <div class="col-sm-6">
         <table class="table table-hover table-striped table-bordered">
            <tbody>
               <?php 
                  echo form_open('/usuario/insert_persona_usuario');
                  
                  $p_nombre = array(
                     'name'=>'p_nombre', 
                     'placeholder'=>'Primer Nombre', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'p_nombre'
                  );
                  $s_nombre = array(
                     'name'=>'s_nombre', 
                     'placeholder'=>'Segundo Nombre', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'s_nombre'
                  );
                  $a_paterno = array(
                     'name'=>'a_paterno', 
                     'placeholder'=>'Primer Apellido', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'a_paterno'
                  );
                  $a_materno = array(
                     'name'=>'a_materno', 
                     'placeholder'=>'Segundo Apellido', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'a_materno'
                  );
                  $ci = array(
                     'name'=>'ci', 
                     'placeholder'=>'CI', 
                     'class'=>'form-control', 
                     'type'=>'number', 
                     'id'=>'ci'
                  );
                  $email = array(
                     'name'=>'email', 
                     'placeholder'=>'Email', 
                     'class'=>'form-control', 
                     'type'=>'email', 
                     'id'=>'email'
                  );
                  $telefono = array(
                     'name'=>'telefono', 
                     'placeholder'=>'Teléfono', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'telefono'
                  );
                  $fecha_nacimiento = array(
                     'name'=>'fecha_nacimiento', 
                     'placeholder'=>'Fecha de Nacimiento', 
                     'class'=>'form-control', 
                     'type'=>'date', 
                     'id'=>'fecha_nacimiento'
                  );
                  $nick = array(
                     'name'=>'nick', 
                     'placeholder'=>'Nick', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'nick'
                  );
                  $pass = array(
                     'name'=>'pass', 
                     'placeholder'=>'Pass', 
                     'class'=>'form-control', 
                     'type'=>'password', 
                     'id'=>'pass'
                  );
                  $rol = array(
                     '0'=>'seleccione',
                     '1'=>'Analista',
                     '2'=>'Administrador'
                  );
                  $activo = array(
                     '0'=>'Inactivo',
                     '1'=>'Activo',
                     '2'=>'Por Aprobación'
                  );
                  $creado_por = array(
                     'name'=>'creado_por', 
                     'placeholder'=>'Para auditoria', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'creado_por'
                  );
                  $modificado_por = array(
                     'name'=>'modificado_por', 
                     'placeholder'=>'Para auditoria', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'modificado_por'
                  );
                   $fecha_creacion = array(
                     'name'=>'fecha_creacion', 
                     'placeholder'=>'yyyy/mm/dd', 
                     'class'=>'form-control', 
                     'type'=>'date', 
                     'id'=>'fecha_creacion', 
                     'data-inputmask'=>"'alias': 'yyyy/mm/dd'",
                     'data-mask'=>""
                  );
                    $fecha_modificacion = array(
                     'name'=>'fecha_modificacion', 
                     'placeholder'=>'yyyy/mm/dd', 
                     'class'=>'form-control', 
                     'type'=>'date', 
                     'id'=>'fecha_modificacion', 
                     'data-inputmask'=>"'alias': 'yyyy/mm/dd'",
                     'data-mask'=>""
                  );
                  $submit = array(
                     'name'=>'submit', 
                     'class'=>'btn btn-success', 
                     'value'=>'Crear Persona'
                  );
               ?>
               <tr>
                  <td><?php echo form_label('Primer Nombre: ', 'p_nombre'); ?></td>
                  <td><?php echo form_input($p_nombre); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Segundo Nombre: ', 's_nombre'); ?></td>
                  <td><?php echo form_input($s_nombre); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Primer Apellido: ', 'a_paterno'); ?></td>
                  <td><?php echo form_input($a_paterno); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Segundo Apellido: ', 'a_materno'); ?></td>
                  <td><?php echo form_input($a_materno); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('CI: ', 'ci'); ?></td>
                  <td><?php echo form_input($ci); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Email: ', 'email'); ?></td>
                  <td><?php echo form_input($email); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Teléfono: ', 'telefono'); ?></td>
                  <td><?php echo form_input($telefono); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Fecha de Nacimiento: ', 'fecha_nacimiento'); ?></td>
                  <td><?php echo form_input($fecha_nacimiento); ?></td>
               </tr>
               <tr>
                  <td><hr></td>
                  <td><hr></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Nombre de Usuario: ', 'nick'); ?></td>
                  <td><?php echo form_input($nick); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Contraseña: ', 'pass'); ?></td>
                  <td><?php echo form_input($pass); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Rol: ', 'rol'); ?></td>
                  <td><?php echo form_dropdown('rol', $rol, false, array('class'=>'form-control')); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Activo: ', 'activo'); ?></td>
                  <td><?php echo form_dropdown('activo', $activo, false, array('class'=>'form-control')); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Creado por: ', 'creado_por'); ?></td>
                  <td><?php echo form_input($creado_por); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Fecha de Creación: ', 'fecha_creacion'); ?></td>
                  <td><?php echo form_input($fecha_creacion); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Modificado por: ', 'modificado_por'); ?></td>
                  <td><?php echo form_input($modificado_por); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Fecha de Modificación: ', 'fecha_modificacion'); ?></td>
                  <td><?php echo form_input($fecha_modificacion); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_submit($submit); ?></td>
                  <td></td>
               </tr>
               <?php 
                  echo form_close();
               ?>
            </tbody>
         </table>
      </div>
   </div>
</div>