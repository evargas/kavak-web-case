<div  class="content-wrapper well">
   <h1>Registro de Nuevo Usuario</h1>
   <div class="row">
      <div class="col-sm-6">
         <table class="table table-hover table-striped table-bordered ">
            <tbody>
               <?php 
                  echo form_open('/usuario/insert_usuario');
                  $id_persona[0] = 'Seleccione';
                  foreach ($personas as $persona) {
                     $id_persona[$persona->id_persona] = $persona->p_nombre; 
                  }
                  $nick = array(
                     'name'=>'nick', 
                     'placeholder'=>'Nick', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'nick'
                  );
                  $pass = array(
                     'name'=>'pass', 
                     'placeholder'=>'Pass', 
                     'class'=>'form-control', 
                     'type'=>'password', 
                     'id'=>'pass'
                  );
                  $rol = array(
                     '0'=>'seleccione',
                     '1'=>'Analista',
                     '2'=>'Administrador'
                  );
                  $activo = array(
                     '0'=>'Inactivo',
                     '1'=>'Activo',
                     '2'=>'Por Aprobación'
                  );
                  $creado_por = array(
                     'name'=>'creado_por', 
                     'placeholder'=>'Para auditoria', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'creado_por'
                  );
                  $modificado_por = array(
                     'name'=>'modificado_por', 
                     'placeholder'=>'Para auditoria', 
                     'class'=>'form-control', 
                     'type'=>'text', 
                     'id'=>'modificado_por'
                  );
                   $fecha_creacion = array(
                     'name'=>'fecha_creacion', 
                     'placeholder'=>'yyyy/mm/dd', 
                     'class'=>'form-control', 
                     'type'=>'date', 
                     'id'=>'fecha_creacion', 
                     'data-inputmask'=>"'alias': 'yyyy/mm/dd'",
                     'data-mask'=>""
                  );
                    $fecha_modificacion = array(
                     'name'=>'fecha_modificacion', 
                     'placeholder'=>'yyyy/mm/dd', 
                     'class'=>'form-control', 
                     'type'=>'date', 
                     'id'=>'fecha_modificacion', 
                     'data-inputmask'=>"'alias': 'yyyy/mm/dd'",
                     'data-mask'=>""
                  );

                  $submit = array(
                     'name'=>'submit', 
                     'class'=>'btn btn-success', 
                     'value'=>'Crear Usuario'
                  );
               ?>
               <tr>
                  <td><?php echo form_label('Id Persona: ', 'id_persona'); ?></td>
                  <td><?php echo form_dropdown('id_persona', $id_persona, false, array('class'=>'form-control')); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Nombre de Usuario: ', 'nick'); ?></td>
                  <td><?php echo form_input($nick); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Contraseña: ', 'pass'); ?></td>
                  <td><?php echo form_input($pass); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Rol: ', 'rol'); ?></td>
                  <td><?php echo form_dropdown('rol', $rol, false, array('class'=>'form-control')); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Activo: ', 'activo'); ?></td>
                  <td><?php echo form_dropdown('activo', $activo, false, array('class'=>'form-control')); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Creado por: ', 'creado_por'); ?></td>
                  <td><?php echo form_input($creado_por); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Fecha de Creación: ', 'fecha_creacion'); ?></td>
                  <td><?php echo form_input($fecha_creacion); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Modificado por: ', 'modificado_por'); ?></td>
                  <td><?php echo form_input($modificado_por); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_label('Fecha de Modificación: ', 'fecha_modificacion'); ?></td>
                  <td><?php echo form_input($fecha_modificacion); ?></td>
               </tr>
               <tr>
                  <td><?php echo form_submit($submit); ?></td>
                  <td></td>
               </tr>
               <?php 
                  echo form_close();
               ?>
            </tbody>
         </table>
      </div>
   </div>
</div>