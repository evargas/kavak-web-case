<fieldset>
	<legend>Buscador</legend>

	<?php 
		echo form_open('/usuario/buscar/');

		$buscar = array(
		 'name'=>'buscar', 
		 'placeholder'=>'Buscador', 
		 'class'=>'form-control', 
		 'type'=>'text', 
		 'id'=>'buscar'
		);
		$submit = array(
		 'name'=>'submit', 
		 'class'=>'btn btn-default', 
		 'value'=>'Buscar Usuario'
		);
		echo form_label('¿A quién quieres encontrar?: ', 'buscar');
		echo form_input($buscar);
		echo '<br>';
		echo form_submit($submit); 

		echo form_close();
	?>
</fieldset>