<div  class="content-wrapper well">
	<h1>Bienvenido</h1>
	<div class="row wow-delay">
		<div class="col-md-3 animated zoomInUp">
		<a href="<?php echo base_url('persona'); ?>">
			<div class="info-box bg-aqua">
			  <span class="info-box-icon"><i class="fa fa-users"></i></span>
			  <div class="info-box-content">
			    <span class="info-box-text">Personas</span>
			    <span class="info-box-number"><?php echo $this->db->count_all('persona'); ?></span>
			    <!-- The progress section is optional -->
			    <div class="progress">
			      <div class="progress-bar" style="width: 20%"></div>
			    </div>
			    <span class="progress-description">
			      20% Increase in 30 Days
			    </span>
			  </div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
		</a>
		</div>
		<div class="col-md-3 animated zoomInUp">
		<a href="<?php echo base_url('usuario'); ?>">
			<div class="info-box bg-yellow">
			  <span class="info-box-icon"><i class="fa fa-user"></i></span>
			  <div class="info-box-content">
			    <span class="info-box-text">Usuarios</span>
			    <span class="info-box-number"><?php echo $this->db->count_all('usuario'); ?></span>
			    <!-- The progress section is optional -->
			    <div class="progress">
			      <div class="progress-bar" style="width: 50%"></div>
			    </div>
			    <span class="progress-description">
			      50% Increase in 30 Days
			    </span>
			  </div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
			</a>
		</div>
		<div class="col-md-3 animated zoomInUp">
		<a href="<?php echo base_url('producto'); ?>">
			<div class="info-box bg-green">
			  <span class="info-box-icon"><i class="fa fa-star"></i></span>
			  <div class="info-box-content">
			    <span class="info-box-text">Productos</span>
			    <span class="info-box-number"><?php echo $this->db->count_all('producto'); ?></span>
			    <!-- The progress section is optional -->
			    <div class="progress">
			      <div class="progress-bar" style="width: 70%"></div>
			    </div>
			    <span class="progress-description">
			      70% Increase in 30 Days
			    </span>
			  </div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
			</a>
		</div>
		<div class="col-md-3 animated zoomInUp">
		<a href="<?php echo base_url('venta'); ?>">
			<div class="info-box bg-red">
			  <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>
			  <div class="info-box-content">
			    <span class="info-box-text">Ventas</span>
			    <span class="info-box-number">Proximamente</span>
			    <!-- The progress section is optional -->
			    <div class="progress">
			      <div class="progress-bar" style="width: 90%"></div>
			    </div>
			    <span class="progress-description">
			      90% Increase in 30 Days
			    </span>
			  </div><!-- /.info-box-content -->
			</div><!-- /.info-box -->
			</a>
		</div>
	</div>
</div>