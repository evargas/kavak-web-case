<?php 
      /**
      * modelo curoso
      */
      class Usuario_model extends CI_Model
      {
            
            function __construct()
            {
                  parent::__construct();
                  $this->load->database();
            }

            function insert_usuario($data){
                  $fields = array(
                                    'id_persona'=>$data['id_persona'],
                                    'nick'=>$data['nick'],
                                    'pass'=>$data['pass'],
                                    'rol'=>$data['rol'],
                                    'activo'=>$data['activo'],
                                    'creado_por'=>$data['creado_por'],
                                    'fecha_creacion'=>$data['fecha_creacion'],
                                    'modificado_por'=>$data['modificado_por'],
                                    'fecha_modificacion'=>$data['fecha_modificacion'],
                              );
                  $this->db->insert('usuario', $fields);
            }

            function get_usuarios(){
                  $query = $this->db->get('usuario');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }

            function get_usuario($uri){
                  $this->db->where('id_usuario', $uri);
                  $query = $this->db->get('usuario');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  } 
            }
            

            function set_usuario($data){
                  $fields = array(
                                    'nick'=>$data['nick'],
                                    'pass'=>$data['pass'],
                                    'rol'=>$data['rol'],
                                    'activo'=>$data['activo'],
                                    'creado_por'=>$data['creado_por'],
                                    'fecha_creacion'=>$data['fecha_creacion'],
                                    'modificado_por'=>$data['modificado_por'],
                                    'fecha_modificacion'=>$data['fecha_modificacion'],
                              );
                  $this->db->where('id_usuario', $data['id']); // primary
                  $query = $this->db->update('usuario', $fields); // tabla y campos
            }

            function delete_usuario($id){
                  $this->db->delete('usuario', array('id_usuario'=>$id));
            }


            function search_usuarios($buscar){
                  $sql = "
                              SELECT * 
                              FROM usuario 
                              WHERE nick LIKE '%$buscar%' 
                        ";
                  $query = $this->db->query($sql);
                  if ($query->num_rows()>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }
      }
