<?php 
      /**
      * modelo curoso
      */
      class Producto_model extends CI_Model
      {
            
            function __construct()
            {
                  parent::__construct();
                  $this->load->database();
            }

            function insert_producto($data){
                  $fields = array(
                                    'nombre_producto'=>$data['nombre'],
                                    'descripcion_producto'=>$data['descripcion'],
                                    'image'=>$data['image'],
                                    'precio'=>$data['precio'],
                                    'stock'=>$data['stock'],
                                    'creado_por'=>$data['creado_por'],
                                    'fecha_creacion'=>$data['fecha_creacion'],
                                    'modificado_por'=>$data['modificado_por'],
                                    'fecha_modificacion'=>$data['fecha_modificacion'],
                              );
                  $this->db->insert('producto', $fields);
            }

            function get_productos(){
                  $query = $this->db->get('producto');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }

            function get_producto($uri){
                  $this->db->where('id_producto', $uri);
                  $query = $this->db->get('producto');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  } 
            }

            function get_productos_analista($user){
                  $this->db->where('creado_por', $user);
                  $query = $this->db->get('producto');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }

            function get_producto_analista($uri, $user){
                  $this->db->where('id_producto', $uri);
                  $this->db->where('creado_por', $user);
                  $query = $this->db->get('producto');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  } 
            }

            function set_producto($data){
                  $fields = array(
                                    'nombre_producto'=>$data['nombre'],
                                    'descripcion_producto'=>$data['descripcion'],
                                    'image'=>$data['image'],
                                    'precio'=>$data['precio'],
                                    'stock'=>$data['stock'],
                                    'creado_por'=>$data['creado_por'],
                                    'fecha_creacion'=>$data['fecha_creacion'],
                                    'modificado_por'=>$data['modificado_por'],
                                    'fecha_modificacion'=>$data['fecha_modificacion'],
                              );
                  $this->db->where('id_producto', $data['id']); // primary
                  $query = $this->db->update('producto', $fields); // tabla y campos
            }


            function delete_producto($id){
                  $this->db->delete('producto', array('id_producto'=>$id));
            }

            function delete_persona($id){
                  $this->db->delete('persona', array('id_persona'=>$id));
            }


            function search_productos($buscar){
                  $sql = "
                              SELECT * 
                              FROM producto 
                              WHERE nombre_producto LIKE '%$buscar%' 
                              or descripcion_producto LIKE '%$buscar%'
                              or precio LIKE '%$buscar%'
                        ";
                  $query = $this->db->query($sql);
                  if ($query->num_rows()>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }

            function search_productos_analista($id, $buscar){
                  $sql = "
                              SELECT * 
                              FROM producto 
                              WHERE nombre_producto LIKE '%$buscar%' 
                              and creado_por = '$id'
                        ";
                  $query = $this->db->query($sql);
                  if ($query->num_rows()>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }

      }
