<?php 
      /**
      * modelo
      */
      class Login_model extends CI_Model
      {
            
            function __construct()
            {
                  parent::__construct();
                  $this->load->database();
            }

            function verificar_login($data){
                  $this->db->select('u.id_usuario, u.nick, u.rol, u.activo, u.fecha_creacion, p.p_nombre, p.s_nombre');
                  $this->db->from('usuario u');
                  $this->db->join('persona p', 'p.id_persona = u.id_persona');
                  $this->db->where('u.nick', $data['nick']);
                  $this->db->where('u.pass', $data['pass']);

                  $result = $this->db->get();
                  if ($result->num_rows() == 1) {
                        $res = $result->result()[0];
                        $s_login = array(
                              'lms_id'=>$res->id_usuario,
                              'lms_user'=>$res->nick,
                              'lms_rol'=>$res->rol,
                              'lms_activo'=>$res->activo,
                              'lms_nombre'=>$res->p_nombre, 
                              'lms_creacion'=>$res->fecha_creacion
                        );
                        $this->session->set_userdata('login', $s_login);
                        return 1;
                  }else{
                        $this->session->unset_userdata('login');
                        $this->session->sess_destroy();
                        return 0;
                  }
            }

}