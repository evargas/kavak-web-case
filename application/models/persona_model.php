<?php 
      /**
      * modelo
      */
      class Persona_model extends CI_Model
      {
            
            function __construct()
            {
                  parent::__construct();
                  $this->load->database();
            }


            function insert_persona($data){
                  $fields = array(
                                    'p_nombre'=>$data['p_nombre'], 
                                    's_nombre'=>$data['s_nombre'],
                                    'a_paterno'=>$data['a_paterno'], 
                                    'a_materno'=>$data['a_materno'],
                                    'ci'=>$data['ci'], 
                                    'email'=>$data['email'],
                                    'telefono'=>$data['telefono'], 
                                    'fecha_nacimiento'=>$data['fecha_nacimiento']
                              );
                  $this->db->insert('persona', $fields);
                  return $this->db->insert_id();
            }


            function get_personas(){
                  $query = $this->db->get('persona');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }
            
            function get_persona($uri){
                  $this->db->where('id_persona', $uri);
                  $query = $this->db->get('persona');
                  if (count($query->result())>0) {
                        return $query->result();
                  }else{
                        return false;
                  } 
            }


            function set_persona($data){
                  $fields = array(
                                    'p_nombre'=>$data['p_nombre'], 
                                    's_nombre'=>$data['s_nombre'],
                                    'a_paterno'=>$data['a_paterno'], 
                                    'a_materno'=>$data['a_materno'],
                                    'ci'=>$data['ci'], 
                                    'email'=>$data['email'],
                                    'telefono'=>$data['telefono'], 
                                    'fecha_nacimiento'=>$data['fecha_nacimiento']
                              );
                  $this->db->where('id_persona', $data['id']); // primary
                  $query = $this->db->update('persona', $fields); // tabla y campos
            }


            function delete_persona($id){
                  $this->db->delete('persona', array('id_persona'=>$id));
            }


            function search_personas($buscar){
                  $sql = "
                              SELECT * 
                              FROM persona 
                              WHERE p_nombre LIKE '%$buscar%' 
                              or s_nombre LIKE '%$buscar%'
                              or a_paterno LIKE '%$buscar%'
                              or a_materno LIKE '%$buscar%'
                              or ci LIKE '%$buscar%'
                              or telefono LIKE '%$buscar%'
                              or email LIKE '%$buscar%'
                        ";
                  $query = $this->db->query($sql);
                  if ($query->num_rows()>0) {
                        return $query->result();
                  }else{
                        return false;
                  }
            }

      }
