<?php 
/**
* 
*/
class Nav
{
	private $arr;

	function __construct($arr){
		$this->arr = $arr;
	}

	public function crear_nav(){
		$nav  = '<nav>
					<ul>';
		foreach ($this->arr as $option) {
		$nav         .= '<li>
							<a href="'.$option['link'].'">'
								.$option['text'].
							'</a>
						</li>';
		}
		$nav .= 	'</ul>
				</nav>';

		return $nav;
	}
}