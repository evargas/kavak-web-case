<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Producto extends CI_Controller {

   function __construct(){
      parent::__construct();
      $this->load->library('session');
      $this->login_redirect();
      $this->load->helper('form');
      $this->load->model('producto_model');
   }


   function login_redirect(){
      if (!(isset($_SESSION['login']) && ($_SESSION['login']['lms_rol'] ==1) || ($_SESSION['login']['lms_rol']==2))) {
         redirect(base_url());
      }
   }
   
   public function index(){
      $data['segment'] = $this->uri->segment(3);
      if ($_SESSION['login']['lms_rol'] !=2) {
         if (!$data['segment']) {
            $data['productos']=$this->producto_model->get_productos_analista($_SESSION['login']['lms_id']);
         }else{
            $data['productos']=$this->producto_model->get_producto_analista($data['segment'], $_SESSION['login']['lms_id']);
         }
      }else{
         if (!$data['segment']) {
            $data['productos']=$this->producto_model->get_productos();
         }else{
            $data['productos']=$this->producto_model->get_producto($data['segment']);
         }
      }
      
      
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head', $data);
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/producto/ver', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts', $data);
   }

   public function nav(){
      $nav = array(
               array(
                        'link'=>base_url().'producto/ver',
                        'text'=>'Productos'
                  ),
               array(
                        'link'=>base_url().'producto/nuevo',
                        'text'=>'Nuevo'
                  ), 
               array(
                        'link'=>base_url().'producto/editar',
                        'text'=>'Editar Producto'
                  ), 
               array(
                        'link'=>base_url().'producto/buscar',
                        'text'=>'Buscar Producto'
                  ), 
            );
      $this->load->library('nav', $nav);
      $nav = array(
               'nav'=>$this->nav->crear_nav()
            );
      $this->load->view('header/nav', $nav);
   }
   /*
      llama al index
      es solo para mostrar una url amigable
   */
   public function ver(){
      $this->index();
   }

   /*
      carga nav
      carga form de registro
   */
   public function nuevo(){
      $this->load->model('usuario_model');
      $data['usuarios']=$this->usuario_model->get_usuarios();
      $data['producto']=$this->producto_model->get_productos();
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head', $data);
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/producto/nuevo', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts', $data);
   }
   
   public function insert_producto(){
      $path = FCPATH;
      $foto = $_FILES['foto']['name'];
      $ruta = $_FILES['foto']['tmp_name'];
      $destino = FCPATH.'assets/dist/img/upload/'.$foto;
      $img_url = base_url().'assets/dist/img/upload/'.$foto;
      copy($ruta, $destino); 
      $data = array(
            'nombre'=>$this->input->post('nombre'),
            'descripcion'=>$this->input->post('descripcion'), 
            'image'=>$img_url, 
            'precio'=>$this->input->post('precio'),
            'stock'=>$this->input->post('stock'),
            'creado_por'=>$this->input->post('creado_por'),
            'fecha_creacion'=>$this->input->post('fecha_creacion'),
            'modificado_por'=>$this->input->post('modificado_por'),
            'fecha_modificacion'=>$this->input->post('fecha_modificacion')
            );
      $this->producto_model->insert_producto($data);
      $this->ver();
   } 

   public function editar(){
      $data['id'] = $this->uri->segment(3);
      if (isset($data['id'])) {
         if ($_SESSION['login']['lms_rol'] !=2) {
            $data['producto']=$this->producto_model->get_producto_analista($data['segment'], $_SESSION['login']['lms_id']);
         }else{
            $data['producto'] = $this->producto_model->get_producto($data['id']);
         }
         $this->load->model('usuario_model');
         $data['usuarios']=$this->usuario_model->get_usuarios();
         $data['module'] = $this->uri->segment(1);
         $this->load->view('layout/admin/head', $data);
         $this->load->view('layout/admin/header', $data);
         $this->load->view('layout/admin/sidebar_left', $data);
         $this->load->view('modules/producto/editar', $data);
         $this->load->view('layout/admin/footer', $data);
         $this->load->view('layout/admin/footer_scripts', $data);
      }else{
         $this->update_producto();
      }
   }

   public function update_producto(){
      $path = FCPATH;
      $foto = $_FILES['foto']['name'];
      $ruta = $_FILES['foto']['tmp_name'];
      $destino = FCPATH.'assets/dist/img/upload/'.$foto;
      $img_url = base_url().'assets/dist/img/upload/'.$foto;
      copy($ruta, $destino);
      $data = array(
            'nombre'=>$this->input->post('nombre'),
            'descripcion'=>$this->input->post('descripcion'), 
            'image'=>$img_url,
            'precio'=>$this->input->post('precio'),
            'stock'=>$this->input->post('stock'),
            'creado_por'=>$this->input->post('creado_por'),
            'fecha_creacion'=>$this->input->post('fecha_creacion'),
            'modificado_por'=>$this->input->post('modificado_por'),
            'fecha_modificacion'=>$this->input->post('fecha_modificacion')
            );
      $data['id'] = $this->uri->segment(3);
      $this->producto_model->set_producto($data);
      $this->ver();
   }

   public function borrar(){
      $id = $this->uri->segment(3);
      $this->producto_model->delete_producto($id);
      $data['producto']=$this->producto_model->get_productos();
      redirect(base_url('producto'));
   }

   public function buscar(){
      $buscar = $this->input->post('buscar');
      $data['productos']=$this->producto_model->search_productos($buscar);
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head', $data);
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/producto/ver', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts', $data);
   }
}
