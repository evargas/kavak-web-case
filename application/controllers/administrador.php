<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller {

   function __construct(){
      parent::__construct();
      $this->load->library('session');
      $this->login_redirect();
      if ($_SESSION['login']['lms_rol'] !=2) {
         redirect(base_url('analista'));
      }
      $this->load->helper('form');
      $this->load->model('persona_model');
      $this->load->model('usuario_model');
      $this->load->model('producto_model');
   }

   function login_redirect(){
      if (!(isset($_SESSION['login']) && ($_SESSION['login']['lms_rol'] ==1) || ($_SESSION['login']['lms_rol']==2))) {
         redirect(base_url());
      }
   }
   
   public function index(){
      
      $data['module'] = $this->uri->segment(1);
      $data['personas']=$this->persona_model->get_personas();
      $data['productos']=$this->producto_model->get_productos();
      $data['usuario']=$this->usuario_model->get_usuarios();
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/administrador/panel');
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }

}