<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Analista extends CI_Controller {

   function __construct(){
      parent::__construct();
      $this->load->library('session');
      $this->login_redirect();
      $this->load->helper('form');
      $this->load->model('persona_model');
      $this->load->model('usuario_model');
      $this->load->model('producto_model');
   }

   function login_redirect(){
      if (!(isset($_SESSION['login']) && ($_SESSION['login']['lms_rol'] ==1) || ($_SESSION['login']['lms_rol']==2))) {
         redirect(base_url());
      }
   }
   public function index(){
      $data['module'] = $this->uri->segment(1);
      $data['productos']=$this->producto_model->get_productos_analista($_SESSION['login']['lms_id']);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/analista/panel');
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }
   function productos(){
      $this->get_productos();
   }
   public function get_productos(){
      $data['segment'] = $this->uri->segment(3);
      if (!$data['segment']) {
         $data['productos']=$this->producto_model->get_productos_analista($_SESSION['login']['lms_id']);
      }else{
         $data['productos']=$this->producto_model->get_producto_analista($data['segment'], $_SESSION['login']['lms_id']);
      }
      
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head', $data);
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/producto/ver', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts', $data);
   }

   public function perfil(){
      $data['usuarios']=$this->usuario_model->get_usuario($_SESSION['login']['lms_id']);
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head', $data);
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/usuario/ver_todos', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts', $data);
   }

   public function buscar(){
      $buscar = $this->input->post('buscar');
      $data['productos']=$this->producto_model->search_productos_analista($_SESSION['login']['lms_id'],$buscar);
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/producto/ver', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }
}