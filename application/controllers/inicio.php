<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

   function __construct(){
      parent::__construct();
      $this->load->library('session');
      $this->load->helper('form');
      $this->load->library('encryption');
      $this->load->model('login_model');
   }

   public function index(){
      $this->load->view('modules/inicio/login-form');
   }

   public function nav(){
      $nav = array(
               array(
                        'link'=>base_url().'inicio/',
                        'text'=>'Inicio'
                  ),
               array(
                        'link'=>base_url().'inicio/ingresar',
                        'text'=>'Ingresar'
                  ), 
               array(
                        'link'=>base_url().'inicio/salir',
                        'text'=>'Salir'
                  ), 
            );
      $this->load->library('nav', $nav);
      $nav = array(
               'nav'=>$this->nav->crear_nav()
            );
      $this->load->view('header/nav', $nav);
   }

   public function login(){
   	
   		$data['nick'] = $this->input->post('nick');
   		$data['pass'] = sha1($this->input->post('pass'));
   		$result = $this->login_model->verificar_login($data);
   		if ($result ==1) {
            if ($_SESSION['login']['lms_rol'] !=2) {
               redirect(base_url('analista'));
            }else{
   			   redirect(base_url('administrador'));
            }
   		}else{
   			$data['error'] = 'Datos Incorrectos';
      		$this->load->view('modules/inicio/login-form', $data);
   		}
   }
}