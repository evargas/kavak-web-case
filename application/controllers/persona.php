<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Persona extends CI_Controller {

   function __construct(){
      parent::__construct();
      $this->load->library('session');
      $this->login_redirect();
      if ($_SESSION['login']['lms_rol'] !=2) {
         redirect(base_url('analista'));
      }
      $this->load->helper('form');
      $this->load->model('persona_model');
   }

   function login_redirect(){
      if (!(isset($_SESSION['login']) && ($_SESSION['login']['lms_rol'] ==1) || ($_SESSION['login']['lms_rol']==2))) {
         redirect(base_url());
      }
   }
   
   public function index(){
      $data['segment'] = $this->uri->segment(3);
      if (!$data['segment']) {
         $data['personas']=$this->persona_model->get_personas();
      }else{
         $data['personas']=$this->persona_model->get_persona($data['segment']);
      }
      
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/persona/ver_todos', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }

   public function nav(){
      $nav = array(
               array(
                        'link'=>base_url().'persona/ver',
                        'text'=>'Personas'
                  ),
               array(
                        'link'=>base_url().'persona/nuevo',
                        'text'=>'Nuevo Persona'
                  ), 
               array(
                        'link'=>base_url().'persona/nueva_persona_usuario',
                        'text'=>'Nueva Persona/Usuario'
                  ),
               array(
                        'link'=>base_url().'persona/editar',
                        'text'=>'Editar Persona'
                  ), 
               array(
                        'link'=>base_url().'persona/buscar',
                        'text'=>'Buscar Persona'
                  ),
            );
      $this->load->library('nav', $nav);
      $nav = array(
               'nav'=>$this->nav->crear_nav()
            );
      $this->load->view('header/nav', $nav);
   }

   public function ver(){
      $this->index();
   }

   public function nuevo(){
      $data['personas']=$this->persona_model->get_personas();
      
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/persona/nueva', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }

   public function insert_persona(){
      $data = array(
            'p_nombre'=>$this->input->post('p_nombre'),
            's_nombre'=>$this->input->post('s_nombre'), 
            'a_paterno'=>$this->input->post('a_paterno'),
            'a_materno'=>$this->input->post('a_materno'),
            'ci'=>$this->input->post('ci'),
            'email'=>$this->input->post('email'),
            'telefono'=>$this->input->post('telefono'),
            'fecha_nacimiento'=>$this->input->post('fecha_nacimiento')
            );
      $this->persona_model->insert_persona($data);
      $this->ver();
   }

   public function nueva_persona_usuario(){
      
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/persona/nueva_persona_usuario');
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }

   public function insert_persona_usuario(){
      $persona = array(
            'p_nombre'=>$this->input->post('p_nombre'),
            's_nombre'=>$this->input->post('s_nombre'), 
            'a_paterno'=>$this->input->post('a_paterno'),
            'a_materno'=>$this->input->post('a_materno'),
            'ci'=>$this->input->post('ci'),
            'email'=>$this->input->post('email'),
            'telefono'=>$this->input->post('telefono'),
            'fecha_nacimiento'=>$this->input->post('fecha_nacimiento')
            );
      $last_id = $this->persona_model->insert_persona($persona);
      if ($last_id >0){
         $usuario = array(
            'id_persona'=>$last_id,
            'nick'=>$this->input->post('nick'),
            'pass'=>$this->input->post('pass'), 
            'rol'=>$this->input->post('rol'),
            'activo'=>$this->input->post('activo'),
            'creado_por'=>$this->input->post('creado_por'),
            'fecha_creacion'=>$this->input->post('fecha_creacion'),
            'modificado_por'=>$this->input->post('modificado_por'),
            'fecha_modificacion'=>$this->input->post('fecha_modificacion')
            );
         $this->persona_model->insert_usuario($usuario);
      }

      $this->ver();
   }


   public function editar(){
      $data['id'] = $this->uri->segment(3);
      if (isset($data['id'])) {
         $data['personas'] = $this->persona_model->get_persona($data['id']);
         
         $data['module'] = $this->uri->segment(1);
         $this->load->view('layout/admin/head');
         $this->load->view('layout/admin/header', $data);
         $this->load->view('layout/admin/sidebar_left', $data);
         $this->load->view('modules/persona/update', $data);
         $this->load->view('layout/admin/footer', $data);
         $this->load->view('layout/admin/footer_scripts');
      }else{
         $this->update_persona();
      }
   }

   public function update_persona(){
      $data = array(
            'p_nombre'=>$this->input->post('p_nombre'),
            's_nombre'=>$this->input->post('s_nombre'), 
            'a_paterno'=>$this->input->post('a_paterno'),
            'a_materno'=>$this->input->post('a_materno'),
            'ci'=>$this->input->post('ci'),
            'email'=>$this->input->post('email'),
            'telefono'=>$this->input->post('telefono'),
            'fecha_nacimiento'=>$this->input->post('fecha_nacimiento')
            );
      $data['id'] = $this->uri->segment(3);
      $this->persona_model->set_persona($data);
      $this->ver();
   }


   public function borrar(){
      $id = $this->uri->segment(3);
      $this->persona_model->delete_persona($id);
      redirect(base_url('persona/ver'));
   }

   public function buscar(){
      $buscar = $this->input->post('buscar');
      $data['personas']=$this->persona_model->search_personas($buscar);
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/persona/ver_todos', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }
}
