<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

   function __construct(){
      parent::__construct();
      $this->load->library('session');
      $this->login_redirect();
      if ($_SESSION['login']['lms_rol'] !=2) {
         redirect(base_url('analista'));
      }
      $this->load->helper('form');
      $this->load->library('encryption');
      $this->load->model('usuario_model');
      }

   function login_redirect(){
      if (!(isset($_SESSION['login']) && ($_SESSION['login']['lms_rol'] ==1) || ($_SESSION['login']['lms_rol']==2))) {
         redirect(base_url());
      }
   }

   function is_admin(){
      if (!(isset($_SESSION['login']) && ($_SESSION['login']['lms_rol']==2))) {
         redirect(base_url());
      }
   }
   function index(){
      $data['segment'] = $this->uri->segment(3);
      if (!$data['segment']) {
         $data['usuarios']=$this->usuario_model->get_usuarios();
      }else{
         $data['usuarios']=$this->usuario_model->get_usuario($data['segment']);
      }
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head', $data);
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/usuario/ver_todos', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts', $data);
   }

   function nav(){
      $nav = array(
               array(
                        'link'=>base_url().'usuario/ver',
                        'text'=>'Usuarios'
                  ),
               array(
                        'link'=>base_url().'usuario/nuevo',
                        'text'=>'Nuevo Usuario'
                  ), 
               array(
                        'link'=>base_url().'usuario/nueva_persona_usuario',
                        'text'=>'Nueva Persona/Usuario'
                  ),
               array(
                        'link'=>base_url().'usuario/editar',
                        'text'=>'Editar Usuario'
                  ), 
               array(
                        'link'=>base_url().'usuario/buscar',
                        'text'=>'Buscar Usuario'
                  ),
            );
      $this->load->library('nav', $nav);
      $nav = array(
               'nav'=>$this->nav->crear_nav()
            );
      $this->load->view('header/nav', $nav);
   }

   function ver(){
      $this->index();
   }

   function nuevo(){
      $this->load->model('persona_model');
      $data['personas']=$this->persona_model->get_personas();
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/usuario/nuevo_usuario', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }

   function insert_usuario(){
      $this->load->library('encryption');
      $data = array(
            'id_persona'=>$this->input->post('id_persona'),
            'nick'=>$this->input->post('nick'),
            'pass'=>sha1($this->input->post('pass')),
            // 'pass'=>$this->encryption->encrypt($this->input->post('pass')),// pare recuperar decrypt
            'rol'=>$this->input->post('rol'),
            'activo'=>$this->input->post('activo'),
            'creado_por'=>$this->input->post('creado_por'),
            'fecha_creacion'=>$this->input->post('fecha_creacion'),
            'modificado_por'=>$this->input->post('modificado_por'),
            'fecha_modificacion'=>$this->input->post('fecha_modificacion')
            );
      $this->usuario_model->insert_usuario($data);
      $this->ver();
   } 

   function nueva_persona_usuario(){
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/usuario/nueva_persona_usuario', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
      
   }

   function insert_persona_usuario(){
      $persona = array(
            'p_nombre'=>$this->input->post('p_nombre'),
            's_nombre'=>$this->input->post('s_nombre'), 
            'a_paterno'=>$this->input->post('a_paterno'),
            'a_materno'=>$this->input->post('a_materno'),
            'ci'=>$this->input->post('ci'),
            'email'=>$this->input->post('email'),
            'telefono'=>$this->input->post('telefono'),
            'fecha_nacimiento'=>$this->input->post('fecha_nacimiento')
            );
      $last_id = $this->usuario_model->insert_persona($persona);
      if ($last_id >0){
         $usuario = array(
            'id_persona'=>$last_id,
            'nick'=>$this->input->post('nick'),
            'pass'=>sha1($this->input->post('pass')), 
            'rol'=>$this->input->post('rol'),
            'activo'=>$this->input->post('activo'),
            'creado_por'=>$this->input->post('creado_por'),
            'fecha_creacion'=>$this->input->post('fecha_creacion'),
            'modificado_por'=>$this->input->post('modificado_por'),
            'fecha_modificacion'=>$this->input->post('fecha_modificacion')
            );
         $this->usuario_model->insert_usuario($usuario);
      }

      $this->ver();
   }

   function editar(){
      $data['id'] = $this->uri->segment(3);
      if (isset($data['id'])) {
         $data['usuario'] = $this->usuario_model->get_usuario($data['id']);
         $data['module'] = $this->uri->segment(1);
         $this->load->view('layout/admin/head');
         $this->load->view('layout/admin/header', $data);
         $this->load->view('layout/admin/sidebar_left', $data);
         $this->load->view('modules/usuario/update_usuario', $data);
         $this->load->view('layout/admin/footer', $data);
         $this->load->view('layout/admin/footer_scripts');
      }else{
         $this->update_usuario();
      }
   }


   function update_usuario(){
      $data = array(
            'nick'=>$this->input->post('nick'),
            'pass'=>sha1($this->input->post('pass')), 
            'rol'=>$this->input->post('rol'),
            'activo'=>$this->input->post('activo'),
            'creado_por'=>$this->input->post('creado_por'),
            'fecha_creacion'=>$this->input->post('fecha_creacion'),
            'modificado_por'=>$this->input->post('modificado_por'),
            'fecha_modificacion'=>$this->input->post('fecha_modificacion')
            );
      $data['id'] = $this->uri->segment(3);
      $this->usuario_model->set_usuario($data);
      $this->ver();
   }

   function update_curso_select(){
      $data = array(
            'id'=>$this->input->post('id_curso'),
            'nombre'=>$this->input->post('nombre'), 
            'video'=>$this->input->post('video'),
            );
      $this->usuario_model->set_curso($data);
      $this->ver();
   }

   function borrar(){
      $id = $this->uri->segment(3);
      $this->usuario_model->delete_usuario($id);
      $data['usuario']=$this->usuario_model->get_usuarios();
      redirect(base_url('usuario/ver'));
   }

   function buscar(){
      $buscar = $this->input->post('buscar');
      $data['usuarios']=$this->usuario_model->search_usuarios($buscar);
      $data['module'] = $this->uri->segment(1);
      $this->load->view('layout/admin/head');
      $this->load->view('layout/admin/header', $data);
      $this->load->view('layout/admin/sidebar_left', $data);
      $this->load->view('modules/usuario/ver_todos', $data);
      $this->load->view('layout/admin/footer', $data);
      $this->load->view('layout/admin/footer_scripts');
   }
}
